package util

import (
	"github.com/faiface/pixel"

	"image"
	"os"
)

// taken from https://github.com/faiface/pixel/wiki/Drawing-a-Sprite
func LoadPicture(path string) (*pixel.PictureData, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	img, _, err := image.Decode(file)
	if err != nil {
		return nil, err
	}
	pic := pixel.PictureDataFromImage(img)
	return pic, nil
}
