package world

import (
	"fmt"
	"gowolf3d/util"
	"image"
	"image/color"
	"io/ioutil"
	"math"

	"github.com/faiface/pixel"
)

type World struct {
	Grid     [][]Cell
	Textures *image.RGBA
}

type Cell interface {
	IsSolid() bool
	GetTextureIndex() pixel.Vec
	ToString() string
	GetMinimapColor() color.RGBA
}

type EmptyCell struct {
}

type WallCell struct {
	Texture pixel.Vec
}

func (c EmptyCell) GetTextureIndex() pixel.Vec {
	return pixel.Vec{0, 0}
}

func (c WallCell) GetTextureIndex() pixel.Vec {
	return c.Texture
}

func (c EmptyCell) IsSolid() bool {
	return false
}

func (c WallCell) IsSolid() bool {
	return true
}

func (c EmptyCell) GetLine() []byte {
	return nil
}

func (c WallCell) GetLine() []byte {
	return nil
}

func (c EmptyCell) ToString() string {
	return "."
}

func (c WallCell) ToString() string {
	return "#"
}

func (c EmptyCell) GetMinimapColor() color.RGBA {
	return color.RGBA{0, 0, 0, 0}
}

func (c WallCell) GetMinimapColor() color.RGBA {
	return color.RGBA{50, 50, 50, 96}
}

func LetThereBeLight(mapfile string, textureMap map[string]Cell, textureFile string) (*World, error) {

	b, err := ioutil.ReadFile(mapfile)
	if err != nil {
		return nil, err
	}

	// build world from mapfile
	world := World{}
	world.Grid = make([][]Cell, 1)
	world.Grid[0] = make([]Cell, 0)
	x := 0
	y := 0

	for i := 0; i < len(b); i++ {
		if b[i] == '\n' {
			x = 0
			y++
			world.Grid = append(world.Grid, make([]Cell, 0))
		} else {
			if cell, exist := textureMap[string(b[i])]; exist {
				world.Grid[y] = append(world.Grid[y], cell)
			} else {
				world.Grid[y] = append(world.Grid[y], &EmptyCell{})
				fmt.Println("Unknown character" + string(b[i]))
			}

			x++
		}
	}

	// reverse world order of rows so origin is bottom left
	for i, j := 0, len(world.Grid)-1; i < j; i, j = i+1, j-1 {
		world.Grid[i], world.Grid[j] = world.Grid[j], world.Grid[i]
	}

	// load textureFile
	picdata, err := util.LoadPicture("data/wolf3dtextures.png")
	if err != nil {
		panic(err)
	}
	world.Textures = picdata.Image()

	return &world, nil
}

func (world World) OutWorld() {
	for y := len(world.Grid) - 1; y >= 0; y-- {
		for x := 0; x < len(world.Grid[y]); x++ {
			fmt.Printf("%s", world.Grid[y][x].ToString())
		}
		fmt.Printf("\n")
	}
}

// Compute the euclidian distance from position pos, along direction
// dir until encountering a solid obstacle in the world
func (worldmap *World) EuclidianDistance(pos pixel.Vec, dir pixel.Vec) (float64, bool, pixel.Vec, float64) {
	// length of ray from x or y side to next x or y side
	deltaDist := pixel.V(math.Abs(1/dir.X), math.Abs(1/dir.Y))

	// working box on the map as we travel
	working := pixel.Vec{X: math.Floor(pos.X), Y: math.Floor(pos.Y)}

	// compute which direction to step and the distance to the next edge boundry
	var step image.Point
	var sideDist pixel.Vec

	if dir.X < 0 {
		step.X = -1
		sideDist.X = (pos.X - working.X) * deltaDist.X
	} else {
		step.X = 1
		sideDist.X = (working.X + 1.0 - pos.X) * deltaDist.X
	}

	if dir.Y < 0 {
		step.Y = -1
		sideDist.Y = (pos.Y - working.Y) * deltaDist.Y
	} else {
		step.Y = 1
		sideDist.Y = (working.Y + 1.0 - pos.Y) * deltaDist.Y
	}

	hit := false
	atSide := false
	stepCount := 0

	// iterate until we find a wall, with an upper bound to allow wall holes
	for !hit && stepCount < 500 {

		// our step  will be whichever distance is smaller
		if sideDist.X < sideDist.Y {
			sideDist.X += deltaDist.X
			working.X += float64(step.X)
			atSide = false
		} else {
			sideDist.Y += deltaDist.Y
			working.Y += float64(step.Y)
			atSide = true
		}

		hit = worldmap.Grid[int(working.Y)][int(working.X)].IsSolid()
	}

	// calculate euclidian distance projected on camera direction
	var euclidianDist, wallX float64
	if atSide {
		euclidianDist = (working.Y - pos.Y + (1-float64(step.Y))/2) / dir.Y
		wallX = pos.X + euclidianDist*dir.X

	} else {
		euclidianDist = (working.X - pos.X + (1-float64(step.X))/2) / dir.X
		wallX = pos.Y + euclidianDist*dir.Y
	}

	// we're interested in how far across the block we intersect, so discard the integer portion
	_, wallX = math.Modf(wallX)

	return euclidianDist, atSide, working, wallX
}

//TODO: we should probably create a loc->cell and cell->texture method from this one
func (worldmap *World) GetTexture(loc pixel.Vec) image.Image {
	textIndex := worldmap.Grid[int(loc.Y)][int(loc.X)].GetTextureIndex()
	x0 := 64 * int(textIndex.X)
	y0 := 64 * int(textIndex.Y)
	x1 := x0 + 64
	y1 := y0 + 64

	subrectangle := image.Rect(x0, y0, x1, y1)
	return worldmap.Textures.SubImage(subrectangle)
}
