package world

import (
	"image"
	"image/color"

	"github.com/faiface/pixel"
)

type Minimap struct {
	image *image.RGBA
}

func (world World) Minimap() *Minimap {
	m := image.NewRGBA(image.Rect(0, 0, len(world.Grid[0])+1, len(world.Grid)))

	for y, row := range world.Grid {
		for x, cell := range row {
			c := cell.GetMinimapColor()
			if c.A == 255 {
				c.A = 96
			}
			//			fmt.Printf("%v,%v\n", x, len(world)-y-1)
			m.Set(x, y, c)
		}
	}
	/*
				m.Set(int(pos.X), int(pos.Y), color.RGBA{255, 0, 0, 255})

					if as.active {

		m.Set(as.X, as.Y, color.RGBA{255, 255, 255, 255})
					} else {
						m.Set(as.X, as.Y, color.RGBA{64, 64, 64, 255})
					}
	*/

	return &Minimap{image: m}
}

func (m *Minimap) AddPoint(p pixel.Vec, color color.RGBA) {
	m.image.Set(int(p.X), int(p.Y), color)
}

func (m *Minimap) Draw(target pixel.Target, transform pixel.Matrix) {
	buf := pixel.PictureDataFromImage(m.image)

	mc := buf.Bounds().Min.Add(pixel.V(-buf.Rect.W(), buf.Rect.H()))

	mapRot := 0.0
	scale := 3.0
	pixel.NewSprite(buf, buf.Bounds()).
		Draw(target,
			transform.Moved(mc).Rotated(mc, mapRot).ScaledXY(pixel.ZV, pixel.V(-scale*2, scale*2)))

}

func (m *Minimap) GetImage() *image.RGBA {
	return m.image
}
