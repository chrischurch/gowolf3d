package player

import (
	"gowolf3d/world"
	"math"

	"github.com/faiface/pixel"
)

type Player interface {
	GetPos() pixel.Vec
	TurnLeft(float64)
	TurnRight(float64)
	MoveForward(float64)
	MoveBackward(float64)
}

// can we eliminate screen plane?
// http://mathworld.wolfram.com/PerpendicularVector.html
type LocalPlayer struct {
	Direction   pixel.Vec
	Position    pixel.Vec
	ScreenPlane pixel.Vec
	WorldMap    *world.World
}

func (p *LocalPlayer) GetPos() pixel.Vec {
	return p.Position
}

func (p *LocalPlayer) TurnLeft(s float64) {
	oldDirX := p.Direction.X

	p.Direction.X = p.Direction.X*math.Cos(s) - p.Direction.Y*math.Sin(s)
	p.Direction.Y = oldDirX*math.Sin(s) + p.Direction.Y*math.Cos(s)

	oldPlaneX := p.ScreenPlane.X

	p.ScreenPlane.X = p.ScreenPlane.X*math.Cos(s) - p.ScreenPlane.Y*math.Sin(s)
	p.ScreenPlane.Y = oldPlaneX*math.Sin(s) + p.ScreenPlane.Y*math.Cos(s)
}

func (p *LocalPlayer) TurnRight(s float64) {
	oldDirX := p.Direction.X

	p.Direction.X = p.Direction.X*math.Cos(-s) - p.Direction.Y*math.Sin(-s)
	p.Direction.Y = oldDirX*math.Sin(-s) + p.Direction.Y*math.Cos(-s)

	oldPlaneX := p.ScreenPlane.X

	p.ScreenPlane.X = p.ScreenPlane.X*math.Cos(-s) - p.ScreenPlane.Y*math.Sin(-s)
	p.ScreenPlane.Y = oldPlaneX*math.Sin(-s) + p.ScreenPlane.Y*math.Cos(-s)
}

func (p *LocalPlayer) safeMove(direction pixel.Vec, s float64) {
	distToObject, _, _, _ := p.WorldMap.EuclidianDistance(p.Position, direction)
	if distToObject > 0.9 {
		p.Position = p.Position.Add(direction.Scaled(s))
	}
}

func (p *LocalPlayer) MoveForward(s float64) {
	p.safeMove(p.Direction.Project(pixel.V(1, 0)), s)
	p.safeMove(p.Direction.Project(pixel.V(0, 1)), s)
}

func (p *LocalPlayer) MoveBackward(s float64) {
	p.safeMove(p.Direction.Project(pixel.V(1, 0)).Scaled(-1), s)
	p.safeMove(p.Direction.Project(pixel.V(0, 1)).Scaled(-1), s)

}

func (p *LocalPlayer) StrafeLeft(s float64) {
	p.safeMove(p.Direction.Rotated(math.Pi/2).Project(pixel.V(1, 0)), s)
	p.safeMove(p.Direction.Rotated(math.Pi/2).Project(pixel.V(0, 1)), s)
}

func (p *LocalPlayer) StrafeRight(s float64) {
	p.safeMove(p.Direction.Rotated(-1*math.Pi/2).Project(pixel.V(1, 0)), s)
	p.safeMove(p.Direction.Rotated(-1*math.Pi/2).Project(pixel.V(0, 1)), s)
}
