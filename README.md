# Wolf3d-style game in Go!

*A Christmas 2018 project!*

This is a Wolfenstein 3d inspired project, aimed at recreating the
core mechanics to better understand them.  This is not a clone.  At
this point in time, I'm uninterested in properly parsing real wolf3d files.  

Future extensions may include:
* multiplayer
* porting view into Javascript
* Enemies represent system resources like Kubernetes pods (wolfernetes/w8s!)


Current State:
![alt text](docs/screenshot.png "Current Gameplay Screenshot")

Current TODO:
* add door mechanics
* add sounds
* add floors/ceilings
* add enemy model and sprites
* add weapon sprite
* add damage

* Create model for items
* Create model for enemies
* Create sprite sheet for enemies
* Create batch to draw enemies
** Time moves enemies

* Refactor core algorithms into tests
* Take a cleanup pass


All code copyright me and released under BSD-2 license, with special thanks to [pixel](https://godoc.org/github.com/faiface/pixel) tutorials and examples.

Wall sprites are from [textures-resource](https://www.textures-resource.com/pc_computer/wolf3d/texture/1375/) and are copyright their respective owners.
