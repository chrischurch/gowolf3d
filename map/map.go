package world

type Cell interface {
	Intersects() boolean
	GetLine() []byte //??

}

type EmptyCell struct {
	x int
	y int
}

type WallCell struct {
	texture []byte
}
