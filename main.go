package main

import (
	"fmt"
	"gowolf3d/player"
	"gowolf3d/world"
	"image"
	"time"

	"flag"
	"log"
	"os"
	"runtime"
	"runtime/pprof"

	"image/color"
	_ "image/png"

	"golang.org/x/image/colornames"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
)

var (
	width  = 1024
	height = 768
)

func frame(worldmap *world.World, lp *player.LocalPlayer) *image.RGBA {
	buf := image.NewRGBA(image.Rect(0, 0, width, height))

	for x := 0; x < width; x++ {
		//		fmt.Printf("x === %d\n", x)
		//		var step image.Point
		//		var sideDist pixel.Vec

		// x coordinate in camera space
		cameraX := 2*float64(x)/float64(width) - 1

		// compute vector for ray for this visual slice
		rayDir := pixel.V(
			lp.Direction.X+lp.ScreenPlane.X*cameraX,
			lp.Direction.Y+lp.ScreenPlane.Y*cameraX,
		)

		euclidianDist, atSide, objectLocation, wallX := worldmap.EuclidianDistance(lp.Position, rayDir)
		// calculate height of line to draw
		lineHeight := float64(height) / euclidianDist

		//		fmt.Printf("  ==> lineHeight: %+v \n", lineHeight)

		// calculate lowest and heighest pixel
		drawStart := -lineHeight/2 + float64(height)/2
		if drawStart < 0 {
			drawStart = 0
		}

		drawEnd := lineHeight/2 + float64(height)/2
		if drawEnd >= float64(height) {
			drawEnd = float64(height) - 1
		}

		texture := worldmap.GetTexture(objectLocation)

		for y := int(drawStart); y < int(drawEnd)+1; y++ {
			/*			var c color.RGBA
						if atSide {
							c = color.RGBA{200.0, 20.0, 20.0, 0.0}
						} else {
							c = color.RGBA{100.0, 20.0, 20.0, 0.0}
						}
			*/
			texSize := 64
			d := float64(y*256-height*128) + lineHeight*128
			texY := int(((d * float64(texSize)) / lineHeight) / 256)
			texX := int(wallX * float64(texSize))

			c := texture.At(
				texture.Bounds().Min.X+texX,
				texture.Bounds().Min.Y+texY%texSize,
			)

			if atSide {
				/*				c.R = c.R / 2
								c.G = c.G / 2
								c.B = c.B / 2
				*/
			}
			buf.Set(x, y, c)
		}
	}
	return buf
}

func run() {
	//TODO: consider moving map file into CSV file
	//TODO: move this into a config file
	textureMapping := map[string]world.Cell{
		"#": world.WallCell{Texture: pixel.Vec{0, 0}},
		"%": world.WallCell{Texture: pixel.Vec{2, 0}},
		"!": world.WallCell{Texture: pixel.Vec{4, 0}},
		"^": world.WallCell{Texture: pixel.Vec{0, 9}},
		"@": world.WallCell{Texture: pixel.Vec{0, 1}},
		"*": world.WallCell{Texture: pixel.Vec{4, 1}},
		".": world.EmptyCell{},
	}
	//EnemyConfig: grid of movements, fire animations
	//Config file for items
	//COnfig file for enemies
	//  Each of these above link to sprite files
	//Config file for map

	w, err := world.LetThereBeLight("test.map", textureMapping, "data/wolf3dtextures.png")
	if err != nil {
		panic(err)
	}

	hero := &player.LocalPlayer{
		Position:    pixel.Vec{X: 20.0, Y: 5.0},
		Direction:   pixel.Vec{X: -1.0, Y: 0.0},
		ScreenPlane: pixel.Vec{X: 0.0, Y: 0.66},
		WorldMap:    w,
		//(more precisely, the FOV is 2 * atan(0.66/1.0)=66°, which is perfect for a first person shooter game).
		//https://lodev.org/cgtutor/raycasting.html
	}

	w.OutWorld()

	cfg := pixelgl.WindowConfig{
		Title:  "And I'm hungry like a...",
		Bounds: pixel.R(0, 0, float64(width), float64(height)),
		VSync:  true,
	}

	win, err := pixelgl.NewWindow(cfg)
	if err != nil {
		panic(err)
	}
	//	win.SetSmooth(true)

	last := time.Now()

	var (
		altOn   = false
		shiftOn = false
		frames  = 0
		second  = time.Tick(time.Second)
	)

	for !win.Closed() {
		dt := time.Since(last).Seconds()
		last = time.Now()

		win.Clear(colornames.Dimgray)

		speed := 6.0
		if shiftOn {
			speed = 9.0
		}

		if win.Pressed(pixelgl.KeyRight) {
			if altOn {
				hero.StrafeRight(speed * dt)
			} else {
				hero.TurnRight(2.4 * dt)
			}
		}

		if win.Pressed(pixelgl.KeyLeft) {
			if altOn {
				hero.StrafeLeft(speed * dt)
			} else {
				hero.TurnLeft(2.4 * dt)
			}
		}
		if win.Pressed(pixelgl.KeyUp) {
			hero.MoveForward(2 * speed * dt)
		}
		if win.Pressed(pixelgl.KeyDown) {
			hero.MoveBackward(2 * speed * dt)
		}

		if win.JustPressed(pixelgl.KeyLeftAlt) {
			altOn = true
		}

		if win.JustReleased(pixelgl.KeyLeftAlt) {
			altOn = false
		}

		if win.JustPressed(pixelgl.KeyLeftShift) {
			shiftOn = true
		}

		if win.JustReleased(pixelgl.KeyLeftShift) {
			shiftOn = false
		}

		p := pixel.PictureDataFromImage(frame(w, hero))

		//		mapRot := 0.0 //-1.6683362599999894

		c := win.Bounds().Center()
		scale := 3.0
		pixel.NewSprite(p, p.Bounds()).
			Draw(win, pixel.IM.Moved(c).Scaled(c, scale))

		minimap := w.Minimap()

		minimap.AddPoint(hero.GetPos(), color.RGBA{200.0, 200.0, 100.0, 255.0})
		minimap.Draw(win, pixel.IM)

		win.Update()

		frames++
		select {
		case <-second:
			win.SetTitle(fmt.Sprintf("%s | FPS: %d", cfg.Title, frames))
			frames = 0
		default:
		}
	}

}

func main() {
	var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to `file`")
	var memprofile = flag.String("memprofile", "", "write memory profile to `file`")
	flag.Parse()
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal("could not create CPU profile: ", err)
		}
		if err := pprof.StartCPUProfile(f); err != nil {
			log.Fatal("could not start CPU profile: ", err)
		}
		defer pprof.StopCPUProfile()
	}
	pixelgl.Run(run)

	if *memprofile != "" {
		f, err := os.Create(*memprofile)
		if err != nil {
			log.Fatal("could not create memory profile: ", err)
		}
		runtime.GC() // get up-to-date statistics
		if err := pprof.WriteHeapProfile(f); err != nil {
			log.Fatal("could not write memory profile: ", err)
		}
		f.Close()
	}
}
